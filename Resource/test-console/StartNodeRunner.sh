#! /bin/bash


DEFAULT_JAVA_PATH=`which java`
MMT_JAVA_PATH=`grep MMT_JAVA_PATH noderunner.properties | head -n1 | cut -d= -f2`

export JAVA_PATH=$DEFAULT_JAVA_PATH

if [ ! -z "$MMT_JAVA_PATH" ]
then
JAVA_PATH=$MMT_JAVA_PATH
fi

java_version=`$JAVA_PATH -version 2>&1 | awk -F '"' 'NR==1 {print $2}'`

if [[ $java_version == "1.8.0"* ]]
then
echo "Configured with required java version $java_version"
else
echo "Required Java Version is 1.8.0 but configured version is $java_version. Please configure MMT_JAVA_PATH in noderunner.properties" && exit
fi

exec ${JAVA_PATH} -DPROC_NAME=mmtnoderunner -jar node.jar --spring.config.location=noderunner.properties >> noderunner.console.log 2>&1 &